#!/usr/bin/env python3
import sys
import os
import thermkinpy as tkp
import pandas as pd
import numpy as np

# Load all datasets in a tkp object
#x = tkp.DataExtraction.read_files(['data1.csv','data2.csv','data3.csv','data4.csv','dataA.csv','dataB.csv'])
x = tkp.DataExtraction.read_files(['10 K min-1.csv',
                                   '1 K min-1.csv',
                                   '2 K min-1.csv',
                                   '383 K.csv',
                                   '403 K.csv',
                                   '5 K min-1.csv'])
print(x)
# You can optionally plot the datasets to explore them before proceding
# with Activation Energy computation
x.plot()

# Normalize datasets by setting the conversion range and the
# number of points you want the datasets to contain
y = x.normalize_datasets(alpha_min=0.05, alpha_max=0.95, npoints=60)

# Yoy can optionally plot the new dataset
y.plot()

# Compute activation energy and show results
ActEnergy = tkp.KineticTriplet(y)
print(ActEnergy)
ActEnergy.plot()
