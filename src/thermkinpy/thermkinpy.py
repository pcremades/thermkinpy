#!/usr/bin/env python3
#Dependencies
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.stats import linregress
from scipy import optimize
from scipy.stats import t
from scipy.integrate import solve_ivp
from scipy import interpolate
from sigfig import round as rnd2sigfig
from derivative import dxdt
# -------------------------------------------------------------------------------------------------

class DataExtraction():
    """
    Extractor to manipulate raw data to create lists of Data Frames
    that will be used to compute the Activation Energy.
    """
    def __init__(self, listOfDatasets=[], namesOfDatasets=[]):
        """
        Constructor.

        Parameters:    None

        Notes:         It only defines variables.
        """
        self.DFlis          = []              # list of DataFrames containing data
        self.DFnames        = []
        self.Beta           = []              # list of heating rates
        self.BetaCC         = []              # list of correlation coefficient for T vs t

        if len(namesOfDatasets) != len(listOfDatasets):
            print('Warning: number of datasets is different from number of',
                  'names given. Creating names by default.')
            # TODO: check if columms are correct
            namesOfDatasets = [f'dataset_{i}' for i in range(len(listOfDatasets))]

        for DF, DFname in zip(listOfDatasets, namesOfDatasets):
            self.add_dataset(DF, DFname)

    @classmethod
    def read_files(cls, flist, encoding='utf8', header=0, skiprows=[1]):
        """
        Alternative constructor. Reads each TGA file as a pandas DataFrame

        Parameters:    flist : list object containing the paths of the files to
                               be used.

                       encoding : The available encodings for pandas.read_csv()
                                  method. Includes but not limited
                                  to 'utf8', 'utf16','latin1'. For more
                                  information on the python standar encoding:
                                  (https://docs.python.org/3/library/codecs.html#standard-encodings)

                       header : line number containing columns headers

                       skiprows : list-like or int indicating lines to skip
                                  while reading files

                       returns: a DataExtraction object with all the datasets.
                                Names of datasets are set to corresponding filenames.
        """

        print("Files to be used: \n{}\n ".format(flist))
        print('Reading files and creating DataFrames...\n')
        DFlist = []
        for item in flist:
            # Try to determine the separator automatically using python engine
            try:
                DF = pd.read_csv(item,  sep=None, encoding=encoding,
                                 engine='python', header=header,
                                 skiprows=skiprows,)
                DFlist.append(DF)

            except IndexError:
                print('Failed to read csv')

        fnames = [fname[:-4] for fname in flist]
        return cls(DFlist, fnames)

    def add_dataset(self, dataset, datasetName=None):
        """
        This function just appends a new dataset to existing list

        Parameters:    dataset: pandas DataFrame to add to existing
                       list of DataFrames
                       datasetName: name of the dataset to append.
        """
        if isinstance(dataset, pd.DataFrame):
            dataset['1/T'] = 1/dataset['temperature']
            self.DFlis.append(dataset)
            if datasetName is not None:
                self.DFnames.append(datasetName)
            else:
                i = len(self.DFnames)
                self.DFnames.append(f'dataset_{i}',)
        else:
            raise Exception('Found invalid Dataset')

    def compute_heat_rate(self):
        """
        Computes the heating rate for every dataset
        """
        self.BetaCC = []  # Clear arrays in order to update values
        self.Beta = []
        for DF in self.DFlis:
            # computes the heating rate
            LR = linregress(DF['time'].values,
                            DF['temperature'].values)

            self.BetaCC.append(LR.rvalue)
            # multiply by 60 to convert from K/s to K/min
            self.Beta.append(LR.slope*60)

    def compute_dadt(self, dataset=None, window_size=2, order=1):
        """
        Computes the derivative of alpha using Savitzky-Galoy filter
        to reduce noise.
        Parameters:     dataset: int index or str name of the dataset
                        for which to compute the derivative column
                        window_size: number of points of the window for the
                            interpolation (default 2)
                        order: order of the polynom to fit (default 1)

        *Note: default values for window_size and order give Savitsky-Golay
               coefficients identicals to those of 5th order finite difference
               differential method. That is to say that no smoothing is applied.
        """
        DF_indexes = []
        if isinstance(dataset, str):
            DF_indexes.append(self.DFnames.index(dataset))
        elif isinstance(dataset, int):
            DF_indexes.append(dataset)
        elif dataset is None:
            DF_indexes = list(range(len(self.DFlis)))
        else:
            raise Exception('dataser must be of type int or str')

        for DF_index in DF_indexes:
            alpha = self.DFlis[DF_index]['alpha'].values
            time = self.DFlis[DF_index]['time'].values
            dAlpha_dt = dxdt(alpha,
                             time,
                             kind="savitzky_golay",
                             order=order,
                             left=int(window_size/2),
                             right=int(window_size/2),
                             )
            self.DFlis[DF_index]['da/dt'] = dAlpha_dt

    def plot(self, marker='', linestyle='-', fig=None, ax=None):
        """
        Plots the datasets in a new figure or overlays plot
        on a previous figure

        Parameters:     fig: figure object (optional)
                        ax: axis object (optional)

        Returns: fig and ax objects
        """
        # plt.style.use('tableau-colorblind10')
        plt.style.use('seaborn-colorblind')
        plt.rc('font', size=14)
        # markers = ["o","v","x","1","s","^","p","<","

        if fig is None and ax is None:
            fig, ax = plt.subplots(2, 2, figsize=(9, 6))
        else:
            # Create a new canvas and assign a fig.
            # Based on https://stackoverflow.com/questions/31729948/matplotlib-how-to-show-a-figure-that-has-been-closed
            dummy = plt.figure()
            new_manager = dummy.canvas.manager
            new_manager.canvas.figure = fig
            fig.set_canvas(new_manager.canvas)

        for DF, name in zip(self.DFlis, self.DFnames):  # range(len(DFlist)):
            ax[0, 0].plot(DF['temperature'].values,  # Temperature in Kelvin
                          DF['alpha'].values,        # Conversion
                          marker=marker,
                          linestyle=linestyle,
                          label=name,
                          alpha=0.75,
                          )
            ax[0, 0].set_ylabel(r'$\alpha$')
            ax[0, 0].set_xlabel('Temperature [K]')
            ax[0, 0].grid(True)

            ax[0, 1].plot(DF['time'].values,  # Time in seconds
                          DF['alpha'].values,  # Conversion
                          marker=marker,
                          linestyle=linestyle,
                          label=name,
                          alpha=0.75,
                          )
            ax[0, 1].set_ylabel(r'$\alpha$')
            ax[0, 1].set_xlabel('time [s]')
            ax[0, 1].legend(loc='center left',
                            bbox_to_anchor=(1, 0.5),
                            frameon=True,
                            fontsize=8,
                            )
            ax[0, 1].grid(True)

            ax[1, 0].plot(DF['temperature'].values,  # Temperature in Kelvin
                          DF['da/dt'].values,  # Derivative of the conversion as a function of time
                          marker=marker,
                          linestyle=linestyle,
                          label=name,
                          alpha=0.75,
                          )
            ax[1, 0].set_ylabel(r'$d\alpha/dt$')
            ax[1, 0].set_xlabel('Temperature [K]')
            ax[1, 0].grid(True)

            ax[1, 1].plot(DF['time'].values,  # Time in seconds
                          DF['da/dt'].values,  # Derivative of the conversion as a function of time
                          marker=marker,
                          linestyle=linestyle,
                          label=name,
                          alpha=0.75,
                          )
            ax[1, 1].set_ylabel(r'$d\alpha/dt$')
            ax[1, 1].set_xlabel('time [s]')
            ax[1, 1].grid(True)
        fig.tight_layout()
        return fig, ax

    def normalize_datasets(self, alpha_min=0.1, alpha_max=0.9, npoints=None):
        """
        Filters data for a given range of conversion (alpha)

        Parameters:     alpha_min: minimum value of alpha
                        alpha_max: maximum value of alpha
                        npoints: subsampling number of points. Default is None
                                In that case, takes the number of points in the
                                the dataset with least points
        Returns: A DataExtaction object with the filtered datasets
        """
        DFlist = []
        DFlist_downsample = []
        min_num_points = 10000000
        for DF in self.DFlis:
            DF_filter = DF[(DF['alpha'] > alpha_min) &
                           (DF['alpha'] < alpha_max)]
            if len(DF_filter.index) < min_num_points:
                min_num_points = len(DF_filter.index)
            DFlist.append(DF_filter)

        if npoints is None:
            npoints = min_num_points
        else:
            npoints = min(npoints, min_num_points)

        for DF in DFlist:
            DF = DF.sample(n=npoints, random_state=1)
            DF.sort_values(by='time', inplace=True)
            DFlist_downsample.append(DF)

        return DataExtraction(DFlist_downsample, self.DFnames)

    def export_csv(self, prefix='export_'):
        """
        Export each dataset in DataExtraction object to a csv.

        Parameters:     prefix: prefix to be used for file names.
                        By default appends 'export_' as prefix to
                        each dataset name.
        """
        for DF, df_name in zip(self.DFlis, self.DFnames):
            DF.to_csv(prefix+df_name+'.csv', index=False)

    def __repr__(self):
        return "DataExtraction(listOfDatasets=[], namesOfDatasetes=[])"

    def __str__(self):
        """
        Prints a summary of each dataset using Pandas describe() method
        """
        print_string = ''
        for dataframe, name in zip(self.DFlis, self.DFnames):
            print_string += name + '\n'
            print_string += str(dataframe.describe()) + '\n\n'
        return print_string

# ------------------------------------------------------------------------------


class KineticTriplet():
    """
    This class computes the Activation Energy for a given dataset
    using the Šesták–Berggren equation

    Parameters:    dataset: a DataExtraction object
    """
    # Initial arbitrary values of n and m
    n0 = 0.98
    m0 = 0.4

    def __init__(self, dataset: DataExtraction):
        self.dataset = dataset
        self.DF_concat = pd.concat(dataset.DFlis)
        self.DF_concat.sort_values(by='alpha', inplace=True)

        # Definition of initial variable parameters
        # self.c = 1
        # self.cA = 1e4
        # self.Ea = 1
        # self.R = 1
        # self.resMin = 0
        # self.n = 0
        # self.m = 0
        self.compute_activation_energy()

    # Definition of equation of a line
    @staticmethod
    def recta(x, p, b):
        return p*x + b

    @staticmethod
    def calc_lhs(data, n, m):
        min_da_dt = np.min(np.abs(data['da/dt']))
        data['da/dt'].where(data['da/dt'] >= 0, min_da_dt, inplace=True)
        result = np.log(data['da/dt']/(np.power((1-data['alpha']), n) *
                                       np.power(data['alpha'], m)))
        return result

    def error(self, x):
        n = x[0]
        m = x[1]
        lhs_local = self.calc_lhs(self.DF_concat, n, m)
        Pears = np.corrcoef(lhs_local, self.DF_concat['1/T'])[0, 1]
        return 1+Pears

    def compute_activation_energy(self):
        # Minimization function
        self.resMin = optimize.minimize(self.error,
                                        x0=np.array([KineticTriplet.n0,
                                                     KineticTriplet.m0]),
                                        tol=1e-7,
                                        method='Nelder-Mead')
        self.n, self.m = self.resMin.x[0], self.resMin.x[1]
        self.PearsonCC = -(1-self.resMin.fun)

        # Linear regression calculation
        self.lhs = self.calc_lhs(self.DF_concat, self.n, self.m)

        self.popt, self.pcov = optimize.curve_fit(self.recta,
                                                  self.DF_concat['1/T'],
                                                  self.lhs)
        self.Slope = self.popt[0]
        self.Intercept = self.popt[1]
        self.Ea = self.Slope*(-8.314E-3)
        self.cA = np.exp(self.Intercept)

        self.NN = self.lhs.size
        gamma = 0.05
        t_n2 = t.ppf(1-gamma/2, self.NN-2)  # Inverse Cumulative t Distribution Function

        self.var_Slope = self.pcov[0, 0]
        self.var_Intercept = self.pcov[1, 1]

        self.err_Slope = np.sqrt(self.var_Slope)*t_n2
        self.err_Intercept = np.sqrt(self.var_Intercept)*t_n2

        self.Error_Ea = self.err_Slope*(8.314E-3)
        self.Error_A = self.err_Intercept*np.exp(self.Intercept)

    def plot(self):
        """
        Plots the datasets
        """
        #plt.style.use('tableau-colorblind10')
        plt.style.use('seaborn-colorblind')
        # markers = ["o","v","x","1","s","^","p","<","2",">"]
        # Plot of conversion versus termperature
        fig, ax = plt.subplots(1, figsize=(9, 6))
        t_aux = np.array([min(self.DF_concat['1/T']),
                          max(self.DF_concat['1/T'])])
        plt.plot(t_aux, self.recta(t_aux, self.Slope, self.Intercept), '-k')
        for DF, name in zip(self.dataset.DFlis, self.dataset.DFnames):
            max_points = 50  # Maximun number of data points to avoid overoloading the plot
            if len(DF) > max_points:
                DF_sample = DF.sample(max_points, random_state=1)  # Get a subset to avoid overloading the plot
            else:
                DF_sample = DF
            ax.plot(DF_sample['1/T'].values,      # Temperature in Kelvin
                    self.calc_lhs(DF_sample, self.n, self.m),  # mass loss percentage
                    linestyle='',
                    marker='o',
                    markersize=10,
                    label=name,
                    alpha=0.5,
                    )
            ax.set_ylabel(r'$\ln( \frac{d\alpha/dt}{(1-\alpha)^n \alpha^m})$')
            ax.set_xlabel(r'1/T [$K^{-1}$]')
            ax.grid(True)
            ax.legend()
        plt.show()

    def __str__(self):
        return(f'R² = {round((1-self.resMin.fun)**2, 3)}\n'
               f'n = {self.n:.3g}\n'
               f'm = {self.m:.3g}\n'
               f'ln(cA) s^-1 = {rnd2sigfig(self.Intercept, uncertainty=self.err_Intercept)}\n'
               f'Ea (kJ/mol) = {rnd2sigfig(self.Ea, uncertainty=self.Error_Ea)}')


def kinetic_models(n, m, models=None):
    """
    This function plots the conversion function for different
    kinetic models, and the Šesták–Berggren equation for given n and m.

    Parameters:   n, m coefficients for Šesták–Berggren equation
                  models: a subset of models to be plot
    """

    def D1(alpha):
        return 1/2*np.power(alpha, -1)/(1/2*np.power(0.5, -1))

    def D2(alpha):
        return np.power(-np.log(1-alpha), -1)/np.power(-np.log(1-0.5), -1)

    def D3(alpha):
        return ((3/2*np.power((1-alpha), 2/3)/(1-np.power((1-alpha), 1/3))) /
                (3/2*np.power((1-0.5), 2/3)/(1-np.power((1-0.5), 1/3))))

    def F0(alpha):
        return alpha/alpha

    def F1(alpha):
        return (1-alpha)/(1-0.5)

    def F2(alpha):
        return (np.power((1-alpha), 2))/(np.power((1-0.5), 2))

    def F25(alpha):
        return (np.power((1-alpha), 2.5))/(np.power((1-0.5), 2.5))

    def F3(alpha):
        return (np.power((1-alpha), 3))/(np.power((1-0.5), 3))

    def A2(alpha):
        return (2*(1-alpha)*np.power(-np.log(1-alpha), 1/2) /
                (2*(1-0.5)*np.power(-np.log(1-0.5), 1/2)))

    def A3(alpha):
        return ((3*(1-alpha)*np.power(-np.log(1-alpha), (2/3))) /
                (3*(1-0.5)*np.power(-np.log(1-0.5), (2/3))))

    def A4(alpha):
        return ((4*(1-alpha)*np.power(-np.log(1-alpha), (3/4))) /
                (4*(1-0.5)*np.power(-np.log(1-0.5), (3/4))))

    def R2(alpha):
        return (np.power((1-alpha), 1/2))/(np.power((1-0.5), 1/2))

    def R3(alpha):
        return (np.power((1-alpha), 2/3))/(np.power((1-0.5), 2/3))
  
    def P3(alpha):
        return (np.power((alpha), 2/3))/(np.power((0.5), 2/3))
        
    def P2(alpha):
        return (np.power((alpha), 1/2))/(np.power((0.5), 1/2))
        
    def L2(alpha):
        return (((np.power(alpha,0.5))-alpha)/((np.power(0.5,0.5))-0.5))

    # Creates a list of kinetic models based on local definitions
    list_models = [value for key, value in locals().items() if callable(value)]

    if models is not None:
        models_to_plot = [model_func for model_func in list_models if model_func.__name__ in models]
    else:
        models_to_plot = list_models

    def SB(alpha, n, m):
        return (((np.power((1-alpha), round(n, 3)))*(np.power(alpha, round(m, 3)))) /
                ((np.power((1-0.5), round(n, 3)))*(np.power(0.5, round(m, 3)))))

    alpha = np.linspace(start=0.1, stop=0.9, num=30)
    plt.style.use('tableau-colorblind10')

    fig, ax = plt.subplots(1, figsize=(9, 6))

    for func in models_to_plot:
        ax.plot(alpha, func(alpha), label=func.__name__)

    ax.plot(alpha,
            SB(alpha, n, m),
            'o',
            markerfacecolor='red',
            markeredgecolor='black',
            label="SB",
            )
    ax.set_ylim(0, 3)
    ax.legend()
    plt.xlabel(r'${\alpha}$')
    plt.ylabel(r'${f(\alpha)/f(0.5)}$')
    plt.show()

    if models is None:
        print()
        MSE = [np.sum((model(alpha) - SB(alpha, n, m))**2) /
               len(alpha) for model in list_models]
        top_5 = pd.DataFrame({'Model': [model.__name__ for model in list_models],
                              'MSE': MSE}).set_index('Model')
        print('List of best fitting models')
        print(top_5.sort_values(by=['MSE']).head(5))


def integrateSB(n, m, cA, E, time_eval, temperature):
    """
    This function returns the integral over time of the Šesták–Berggren
    differential equation over a given time interval and a temperature
    profile

    Parameters:   n, m: coefficients for Šesták–Berggren equation
                  TODO: cA: (consultar al Alex la definición)
                  E: Activation energy
                  time_eval: a list of times at which to compute the solution
                  temperature: temperature at each time in the time_eval list

    Returns:      resultado.y[0]: α(t) evaluated at times in time_eval
    """
    R = 8.314e-3
    if isinstance(time_eval, pd.core.series.Series):
        time_eval = time_eval.tolist()

    TempProgram = interpolate.interp1d(time_eval, temperature)

    # Šesták–Berggren differential equation
    def dα_dt(time, α):
        return (cA*np.exp(-E / (R * TempProgram(time))) *
                (np.power((1-α), n)) *
                (np.power(α, m)))

    α_ini = 0.0001

    resultado = solve_ivp(dα_dt,
                          t_span = [time_eval[0], time_eval[-1]],
                          y0=[α_ini],
                          t_eval=time_eval,
                          method='RK45',
                          first_step=0.0001,
                          )

    return resultado

def reconstruct(ds, kt):
    """
    Creates a dataset by integrating the Šesták–Berggren equation
    given the exponents in the kinetic triplet kt and for the times
    and temperature profile in dataset ds.

    Parameters:     ds: the original experimental dataset used in
                        the kinetic triplet analisys.
                    kt: the kinetic triplet object obtained in the
                        ds dataset analysis.
    """
    if not isinstance(ds, DataExtraction):
        raise Exception('ds must be instance of DataExtraction')
    if not isinstance(kt, KineticTriplet):
        raise Exception('kt must be instance of KineticTriplet')

    reconstruct = []
    for DF, DFname in zip(ds.DFlis, ds.DFnames):
        time_eval = DF['time']
        temp = DF['temperature']
        resultado = integrateSB(n=kt.n,
                                m=kt.m,
                                cA=kt.cA,
                                E=kt.Ea,
                                temperature=temp,
                                time_eval=time_eval,
                                )

        dummyDF = pd.DataFrame({'time': time_eval,
                                'temperature': temp,
                                'alpha': resultado.y[0],
                                }
                               )

        names = [name+'_reconstruct' for name in ds.DFnames]

        reconstruct.append(dummyDF)

    reconstructObj = DataExtraction(reconstruct, names)
    for DFname in reconstructObj.DFnames:
        reconstructObj.compute_dadt(DFname)

    return reconstructObj
